export interface LoginUser {
  email: string;
  password: string;
}

export interface User extends LoginUser {
  id: string;
  name: string;
  surname: string;
}
