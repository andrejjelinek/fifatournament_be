export interface FutabllStats {
  id: number;
  playerName: string;
  squadName: string;
  sessionDate: Date;
  sessionName: string;
  totalDistance: number | null;
  dsl: number | null;
  hidForMin: number | null;
  hsrForMin: number | null;
  maxHr: number | null;
  maxSpeed: number | null;
  countOfSprints: number | null;
  averageHr: number | null;
  slowingDown: number | null;
  stepBalanceL: number | null;
  stepBalanceR: number | null;
  highSpeedRunning: number | null;
  distanceForMin: number | null;
  distanceOfSprint: number | null;
  distanceOfSprintForMin: number | null;
  calories: number | null;
  acceleration: number | null;
  impacts: number | null;
  timeInRedZone: Date;
  highIntensityDistance: number | null;
}
