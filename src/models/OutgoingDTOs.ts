import { MatchType } from "./IncomingDTOs";

export interface MatchOutDto {
  id: string;
  groupId: string;
  player1: PlayerOutDto;
  player2: PlayerOutDto;
  hasWinner: boolean;
  isFromLoosed: boolean;
}

export interface PlayerOutDto {
  id: string;
  name: string;
  goals: number;
}

export interface MatchTypeOutDto {
  id: string;
  matchType: TypeOfMatchesOutDto;
  countOfMatches: number;
  matches: MatchOutDto[];
}

export interface TypeOfMatchesOutDto {
  id: number;
  type: MatchType;
  name: string;
  countOfMatches: number;
}

export interface TournamentOutDto {
  id: string;
  name: string;
  date: string;
  description: string;
  countOfPlayers: number;
}

export interface PlayerScoreOutDto {
  playerId: string;
  playerName: string;
  points: number;
}
