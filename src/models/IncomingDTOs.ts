import { PlayerOutDto, TypeOfMatchesOutDto } from "./OutgoingDTOs";

export interface NewMatchInDto {
  firstPlayerName: string;
  secondPlayerName: string;
  tournamentId: string;
  matchType: TypeOfMatchesOutDto;
  groupId: string;
}

export interface UpdateMatchInDto {
  matchId: string;
  firstPlayer: PlayerOutDto;
  secondPlayer: PlayerOutDto;
  pointsPlayer1: number;
  pointsPlayer2: number;
  matchType: MatchType;
}

export interface UpdatePlayerInDto {
  id: string;
  name: string;
}

export interface NewTournamentInDto {
  name: string;
  date: string;
  description: string;
}

export enum MatchType {
  basic = "BASIC",
  eightFinal = "EIGHT_FINAL",
  quaterFinal = "QUATER_FINAL",
  semiFinal = "SEMI_FINAL",
  final = "FINAL",
}
