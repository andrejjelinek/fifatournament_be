export interface ServiceResponse<T> {
  message: string;
  success: boolean;
  data: T | null;
}
