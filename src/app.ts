import cors from "cors";
import * as dotenv from "dotenv";
import express, { Express, NextFunction, Request, Response } from "express";
import { ServiceResponse } from "./models/ServiceResponse";
import router from "./routes/routes";

dotenv.config();
const app: Express = express();
const port = 3000;

app.use(cors());
app.use(express.json());
app.use(router);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.log(err);
  const response: ServiceResponse<string> = {
    message: "ERROR - something went wrong on the server!",
    success: false,
    data: null,
  };
  res.status(500).json(response);
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
