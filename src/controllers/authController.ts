// import { User } from "../models/authModels";
// import authService from "../services/authService";
// import { Request, Response, NextFunction } from "express";

// /**
//  * Register a new user
//  * @param req
//  * @param res
//  * @param next
//  */
// const registerUser = async (
//   req: Request<User>,
//   res: Response,
//   next: NextFunction
// ) => {
//   try {
//     const user = req.body as User;
//     res.status(200).json(await authService.registerUser(user));
//   } catch (err) {
//     next(err);
//   }
// };

// /**
//  * Login user with email and password
//  * @param req
//  * @param res
//  * @param next
//  */
// const login = async (
//   req: Request<{ email: string; password: string }>,
//   res: Response,
//   next: NextFunction
// ) => {
//   try {
//     const email = req.body.email;
//     const pwd = req.body.password;

//     res.status(200).json(await authService.login(email, pwd));
//   } catch (err) {
//     next(err);
//   }
// };

// export default {
//   registerUser,
//   login,
// };
