import { NextFunction, Request, Response } from "express";
import {
  NewMatchInDto,
  NewTournamentInDto,
  UpdateMatchInDto,
  UpdatePlayerInDto,
} from "../models/IncomingDTOs";
import tournamentService from "../services/tournamentService";

const createTournament = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const tournament = req.body as NewTournamentInDto;
    res
      .status(200)
      .json(await tournamentService.createNewTournament(tournament));
  } catch (err) {
    next(err);
  }
};

const getTournaments = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    res.status(200).json(await tournamentService.getTournaments());
  } catch (err) {
    next(err);
  }
};

const getTournamentMatches = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const tournamentId = req.params.tournamentId as string;
    const isFromLoosed = req.params.isFromLoosed == "true";
    res
      .status(200)
      .json(
        await tournamentService.getTournamentMatches(tournamentId, isFromLoosed)
      );
  } catch (err) {
    next(err);
  }
};

const createNewMatch = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const match = req.body as NewMatchInDto;
    res.status(200).json(await tournamentService.createNewMatch(match));
  } catch (err) {
    next(err);
  }
};

const updateMatch = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const match = req.body as UpdateMatchInDto;
    const isFromLoosed = req.params.isFromLoosed === "true";
    res
      .status(200)
      .json(await tournamentService.updateMatch(match, isFromLoosed));
  } catch (err) {
    next(err);
  }
};

const updatePlayersInfo = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const players = req.body as UpdatePlayerInDto[];
    res.status(200).json(await tournamentService.updatePlayersInfo(players));
  } catch (err) {
    next(err);
  }
};

const getPlayersPointByTournament = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const tournamentId = req.params.tournamentId as string;
    const loosedType = req.params.loosedType == "true";

    res
      .status(200)
      .json(
        await tournamentService.getPlayersPointByTournament(
          tournamentId,
          loosedType
        )
      );
  } catch (err) {
    next(err);
  }
};

export default {
  createTournament,
  getTournaments,
  getTournamentMatches,
  createNewMatch,
  updateMatch,
  updatePlayersInfo,
  getPlayersPointByTournament,
};
