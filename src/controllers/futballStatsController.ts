import { NextFunction, Request, Response } from "express";
import multer from "multer";
import csvParser from "csv-parser";
import { FutabllStats } from "../models/FutabllStatsDTO";
const uploadPlayerData = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const csvData: any[] = [];
    const bufferString = req.file?.buffer?.toString("utf8");

    if (!bufferString) {
      return res.status(400).send("No file uploaded");
    }

    const parser = csvParser({ headers: true, separator: "," });

    parser.on("data", (data) => {
      csvData.push(data);
    });

    parser.on("end", () => {
      console.log("CSV data:", csvData);
      console.log(csvData[1]["_0"]);
      console.log(csvData[1]["_1"]);

      const row = csvData[1];
      if (!row) {
        res.status(500).json({ message: "ERROR - nepodarilo sa získať dáta zo súboru." });
      }

      const futablStats: FutabllStats = {
        id: 0,
        playerName: row["_0"],
        squadName: row["_1"],
        sessionDate: row["_2"],
        sessionName: row["_3"],
        totalDistance: row["_4"],
        highSpeedRunning: row["_5"],
        distanceForMin: row["_6"],
        impacts: row["_7"],
        maxSpeed: row["_8"],
        highIntensityDistance: row["_9"],
        countOfSprints: row["_10"],
        distanceOfSprint: row["_11"],
        acceleration: row["_12"],
        slowingDown: row["_13"],
        calories: row["_14"],
        hsrForMin: row["_15"],
        hidForMin: row["_16"],
        distanceOfSprintForMin: row["_17"],
        stepBalanceL: row["_18"],
        stepBalanceR: row["_19"],
        dsl: row["_20"],
        maxHr: row["_21"],
        averageHr: row["_22"],
        timeInRedZone: row["_23"],
      };
      console.log(futablStats);
      res.status(200).json({ message: "OK BE", data: futablStats });
    });

    parser.on("error", (err) => {
      console.error("Error parsing CSV:", err);
      res.status(500).send("Error parsing CSV");
    });

    parser.write(bufferString);
    parser.end(); // Explicitly call end to ensure proper termination
  } catch (error) {
    console.log("error" + (error as Error).message);
    next(error);
  }
};

export default {
  uploadPlayerData,
};
