import { Match, PrismaClient } from "@prisma/client";
import {
  NewMatchInDto,
  NewTournamentInDto,
  UpdateMatchInDto,
  UpdatePlayerInDto,
} from "../models/IncomingDTOs";
import {
  MatchOutDto,
  MatchTypeOutDto,
  PlayerScoreOutDto,
  TournamentOutDto,
  TypeOfMatchesOutDto,
} from "../models/OutgoingDTOs";
import { ServiceResponse } from "../models/ServiceResponse";

const prisma = new PrismaClient();

/**
 * Create new tournament with blank matches
 * @param tournament
 * @returns
 */
const createNewTournament = async (tournament: NewTournamentInDto) => {
  try {
    const newTournament = await prisma.tournament.create({
      data: {
        name: tournament.name,
        date: new Date(tournament.date),
        description: tournament.description,
        countOfPlayers: 16,
      },
    });

    const response: ServiceResponse<string> = {
      data: newTournament.id,
      message: "OK",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Get array of created tournaments
 * @returns
 */
const getTournaments = async () => {
  try {
    const result = await prisma.tournament.findMany();

    const tournaments: TournamentOutDto[] = result.map((item) => {
      return {
        id: item.id,
        name: item.name,
        date: item.date.toUTCString(),
        description: item.description ?? "",
        countOfPlayers: item.countOfPlayers,
      };
    });

    const response: ServiceResponse<TournamentOutDto[]> = {
      data: tournaments,
      message: "OK",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Get tournament matches by tournament ID
 * @param tournamentId
 * @returns
 */
const getTournamentMatches = async (
  tournamentId: string,
  isFromLoosed: boolean
) => {
  try {
    const test: MatchTypeOutDto[] = [];

    const matchTypes = await prisma.matchType.findMany({
      where: {
        type: {
          not: "BASIC",
        },
      },
    });

    await Promise.all(
      matchTypes.map(async (matchType) => {
        let result = await prisma.match.findMany({
          where: {
            tournamentId: tournamentId,
            matchType: matchType,
            isFromLoosed: isFromLoosed,
          },
          include: {
            ScheduleMatchs: {
              select: {
                id: true,
                player: true,
                goals: true,
              },
            },
            winner: true,
            matchType: true,
          },
          orderBy: [{ groupId: "asc" }, { matchDate: "asc" }],
        });

        if (result) {
          if (isFromLoosed) {
            result = result.filter((item) => item.matchType.typeNumber != 2);
          }

          const matches = result.map((item) => {
            const match: MatchOutDto = {
              id: item.id,
              groupId: item.groupId,
              isFromLoosed: item.isFromLoosed,
              player1: {
                id: item.ScheduleMatchs[0]?.player.id,
                name: item.ScheduleMatchs[0]?.player.name,
                goals: item.ScheduleMatchs[0]?.goals ?? 0,
              },
              player2: {
                id: item.ScheduleMatchs[1]?.player.id,
                name: item.ScheduleMatchs[1]?.player.name,
                goals: item.ScheduleMatchs[1]?.goals ?? 0,
              },
              hasWinner: item.winnerPlayerId != null,
            };
            return match;
          });

          test.push({
            matchType: matchType as TypeOfMatchesOutDto,
            countOfMatches: matchType.countOfMatches,
            id: "asdf",
            matches: matches,
          });
        }
      })
    );

    if (test.length > 0) {
      test.sort((a, b) => a.matchType.id - b.matchType.id);
      const response: ServiceResponse<MatchTypeOutDto[]> = {
        data: test,
        message: "OK",
        success: true,
      };
      return response;
    } else {
      const response: ServiceResponse<MatchTypeOutDto[]> = {
        data: null,
        message: "No matches found!",
        success: false,
      };
      return response;
    }
  } catch (error) {
    throw error;
  }
};

/**
 * Create new match
 * @param match
 * @returns
 */
const createNewMatch = async (match: NewMatchInDto) => {
  try {
    const player1 = await prisma.player.create({
      data: { name: match.firstPlayerName },
    });

    const player2 = await prisma.player.create({
      data: { name: match.secondPlayerName },
    });

    const newMatch = await prisma.match.create({
      data: {
        matchTypeId: match.matchType.id,
        tournamentId: match.tournamentId,
        groupId: match.groupId,
        isFromLoosed: false,
      },
    });

    await prisma.scheduleMatchs.create({
      data: {
        matchId: newMatch.id,
        playerId: player1.id,
      },
    });

    await prisma.scheduleMatchs.create({
      data: {
        matchId: newMatch.id,
        playerId: player2.id,
      },
    });

    const result: MatchOutDto = {
      id: newMatch.id,
      groupId: match.groupId,
      player1: { id: player1.id, name: player1.name, goals: 0 },
      player2: { id: player2.id, name: player2.name, goals: 0 },
      hasWinner: newMatch.winnerPlayerId != null,
      isFromLoosed: newMatch.isFromLoosed,
    };

    const response: ServiceResponse<MatchOutDto> = {
      data: result,
      message: "OK",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Update players names
 * @param players
 * @returns
 */
const updatePlayersInfo = async (players: UpdatePlayerInDto[]) => {
  try {
    await Promise.all(
      players.map(async (item) => {
        let playerDb = await prisma.player.findUnique({
          where: {
            id: item.id,
          },
        });

        if (playerDb && playerDb.name != item.name) {
          await prisma.player.update({
            where: { id: item.id },
            data: { name: item.name },
          });
        }
      })
    );

    const response: ServiceResponse<string> = {
      data: "OK",
      message: "OK",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Update match score and points for players
 * @param matchToUpdate
 * @param isFromLoosed
 * @returns
 */
const updateMatch = async (
  matchToUpdate: UpdateMatchInDto,
  isFromLoosed: boolean
) => {
  try {
    const match = await prisma.match.findUnique({
      where: { id: matchToUpdate.matchId },
    });

    if (!match) {
      const response: ServiceResponse<MatchOutDto> = {
        data: null,
        message: "Nebol nájdený zápas na úpravu!",
        success: false,
      };
      return response;
    }

    //find scheduled matches for players and if score change update goals
    const matchPlayer1 = await prisma.scheduleMatchs.findFirst({
      where: {
        matchId: matchToUpdate.matchId,
        playerId: matchToUpdate.firstPlayer.id,
      },
    });
    const matchPlayer2 = await prisma.scheduleMatchs.findFirst({
      where: {
        matchId: matchToUpdate.matchId,
        playerId: matchToUpdate.secondPlayer.id,
      },
    });

    let isScoreUpdated = false;
    if (matchPlayer1 && matchPlayer1.goals != matchToUpdate.firstPlayer.goals) {
      isScoreUpdated = true;
      await prisma.scheduleMatchs.update({
        where: {
          id: matchPlayer1.id,
        },
        data: {
          goals: matchToUpdate.firstPlayer.goals,
        },
      });
      await updateScore(
        match?.tournamentId,
        matchToUpdate.firstPlayer.id,
        matchToUpdate.pointsPlayer1,
        isFromLoosed
      );
    }
    if (
      matchPlayer2 &&
      matchPlayer2.goals != matchToUpdate.secondPlayer.goals
    ) {
      isScoreUpdated = true;
      await prisma.scheduleMatchs.update({
        where: {
          id: matchPlayer2.id,
        },
        data: {
          goals: matchToUpdate.secondPlayer.goals,
        },
      });
      await updateScore(
        match?.tournamentId,
        matchToUpdate.secondPlayer.id,
        matchToUpdate.pointsPlayer2,
        isFromLoosed
      );
    }

    //if is score updated update winner for match
    let winnerId: string = "";
    let looserId: string = "";
    if (isScoreUpdated) {
      if (matchToUpdate.firstPlayer.goals > matchToUpdate.secondPlayer.goals) {
        winnerId = matchToUpdate.firstPlayer.id;
        looserId = matchToUpdate.secondPlayer.id;
      } else {
        winnerId = matchToUpdate.secondPlayer.id;
        looserId = matchToUpdate.firstPlayer.id;
      }
    }

    const result = await prisma.match.update({
      where: { id: matchToUpdate.matchId },
      data: {
        winnerPlayerId: winnerId,
      },
      include: {
        matchType: true,
      },
    });

    if (matchToUpdate.matchType === "FINAL") {
      const response: ServiceResponse<MatchOutDto> = {
        data: null,
        message: "OK - všetko prebehlo v poriadku",
        success: true,
      };

      return response;
    } else if (matchToUpdate.matchType === "EIGHT_FINAL") {
      await updateCreateMatchInNextMatchType(
        result,
        winnerId,
        result.matchType.typeNumber,
        false
      );

      await updateCreateMatchInNextMatchType(
        result,
        looserId,
        result.matchType.typeNumber,
        true
      );
    } else if (isFromLoosed) {
      await updateCreateMatchInNextMatchType(
        result,
        winnerId,
        result.matchType.typeNumber,
        true
      );
    } else {
      await updateCreateMatchInNextMatchType(
        result,
        winnerId,
        result.matchType.typeNumber,
        false
      );
    }

    const response: ServiceResponse<MatchOutDto> = {
      data: null,
      message: "OK - všetko prebehlo v poriadku",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

/**
 * Create new match in next match type
 * @param match
 * @param playerId
 * @param typeNumber
 * @param isLoosed
 * @returns
 */
const updateCreateMatchInNextMatchType = async (
  match: Match,
  playerId: string,
  typeNumber: number,
  isLoosed: boolean
) => {
  const newGroupId = getNextMatchTypeGroupId(match.groupId);
  const nextMatchType = await prisma.matchType.findFirst({
    where: {
      typeNumber: typeNumber + 1,
    },
  });

  if (!nextMatchType) {
    return;
  }

  const result = await prisma.match.findMany({
    where: {
      groupId: newGroupId,
      matchTypeId: nextMatchType.id,
      isFromLoosed: isLoosed,
    },
    include: {
      ScheduleMatchs: {
        select: {
          id: true,
          player: true,
        },
        where: {
          playerId: playerId,
        },
      },
    },
  });

  if (result.length === 1) {
    await prisma.scheduleMatchs.create({
      data: {
        matchId: result[0].id,
        playerId: playerId,
      },
    });
  } else if (result.length === 2) {
  } else {
    const newMatch = await prisma.match.create({
      data: {
        matchTypeId: nextMatchType.id,
        tournamentId: match.tournamentId,
        groupId: newGroupId,
        isFromLoosed: isLoosed,
      },
    });

    await prisma.scheduleMatchs.create({
      data: {
        matchId: newMatch.id,
        playerId: playerId,
      },
    });
  }
};

/**
 * Get next match type
 * @param actualGroupId
 * @returns
 */
const getNextMatchTypeGroupId = (actualGroupId: string) => {
  if (actualGroupId === "1-a" || actualGroupId === "1-b") {
    return "1-a";
  } else if (actualGroupId === "2-a" || actualGroupId === "2-b") {
    return "1-b";
  } else if (actualGroupId === "3-a" || actualGroupId === "3-b") {
    return "2-a";
  } else if (actualGroupId === "4-a" || actualGroupId === "4-b") {
    return "2-b";
  } else if (actualGroupId === "5-a" || actualGroupId === "5-b") {
    return "3-a";
  } else if (actualGroupId === "6-a" || actualGroupId === "6-b") {
    return "3-b";
  } else if (actualGroupId === "7-a" || actualGroupId === "7-b") {
    return "4-a";
  } else {
    return "4-b";
  }
};

/**
 * Update score for players
 * @param tournamentId
 * @param playerId
 * @param points
 * @param isFromLoosed
 */
const updateScore = async (
  tournamentId: string,
  playerId: string,
  points: number,
  isFromLoosed: boolean
) => {
  try {
    const score = await prisma.score.findUnique({
      where: {
        tournamentId_playerId: {
          playerId: playerId,
          tournamentId: tournamentId,
        },
      },
    });
    if (score) {
      await prisma.score.update({
        where: {
          tournamentId_playerId: {
            playerId: playerId,
            tournamentId: tournamentId,
          },
        },
        data: {
          points: score.points + points,
          loosedType: isFromLoosed,
        },
      });
    } else {
      await prisma.score.create({
        data: {
          tournamentId: tournamentId,
          playerId: playerId,
          points: points,
          loosedType: isFromLoosed,
        },
      });
    }
  } catch (error) {
    throw error;
  }
};

/**
 * Get players points
 * @param tournamentId
 * @param isLoosedType
 * @returns
 */
const getPlayersPointByTournament = async (
  tournamentId: string,
  isLoosedType: boolean
) => {
  try {
    const result = await prisma.score.findMany({
      where: {
        tournamentId: tournamentId,
        loosedType: isLoosedType,
      },
      include: {
        player: true,
      },
      orderBy: {
        points: "desc",
      },
    });

    const playersPoint: PlayerScoreOutDto[] = result.map((item) => {
      return {
        playerId: item.playerId,
        playerName: item.player.name,
        points: item.points,
      };
    });

    const response: ServiceResponse<PlayerScoreOutDto[]> = {
      data: playersPoint,
      message: "OK",
      success: true,
    };

    return response;
  } catch (error) {
    throw error;
  }
};

export default {
  createNewTournament,
  getTournaments,
  getTournamentMatches,
  createNewMatch,
  updateMatch,
  updatePlayersInfo,
  getPlayersPointByTournament,
};
