// import jwt from "jsonwebtoken";
// import { User } from "../models/authModels";
// import bcrypt from "bcrypt";
// import { PrismaClient } from "@prisma/client";
// import { ServiceResponse } from "../models/ServiceResponse";

// const prisma = new PrismaClient();

// /**
//  * Create new user
//  * @param user
//  * @returns
//  */
// const registerUser = async (user: User) => {
//   try {
//     const hashedPwd = await bcrypt.hash(user.password, 12);
//     const newUser = await prisma.user.create({
//       data: {
//         email: user.email,
//         password: hashedPwd,
//         name: user.name,
//         surname: user.surname,
//       },
//     });

//     const response: ServiceResponse<string> = {
//       data: newUser.id,
//       success: true,
//       message: "OK",
//     };
//     return response;
//   } catch (err) {
//     throw err;
//   }
// };

// /**
//  * Login user with password and email
//  * @param email
//  * @param password
//  * @returns
//  */
// const login = async (email: string, password: string) => {
//   try {
//     const user = await prisma.user.findFirst({
//       where: { email: email },
//     });
//     if (!user) {
//       throw new Error("User with this email was not found!");
//     }
//     const isAuthenticated = await bcrypt.compare(password, user.password);

//     if (!isAuthenticated) {
//       throw new Error("Wrong password!");
//     }

//     const acessToekn = jwt.sign(
//       { email: email, id: user.id },
//       process.env.SECRET_TOKEN as string,
//       { expiresIn: "1h" }
//     );

//     const response: ServiceResponse<string> = {
//       data: acessToekn,
//       success: true,
//       message: "OK",
//     };
//     return response;
//   } catch (err) {
//     throw err;
//   }
// };

// export default {
//   registerUser,
//   login,
// };
