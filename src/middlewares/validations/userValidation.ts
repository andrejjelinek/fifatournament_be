import { Request, Response, NextFunction } from "express";
import Joi from "joi";
import { User } from "../../models/authModels";

const registerUserSchema = Joi.object({
  email: Joi.string().email().required().messages({
    "string.base": "Email must be a string!",
    "string.email": "Wrong email!",
    "any.required": "Email is required!",
  }),
  password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")).required(),
  name: Joi.string().min(3).max(20).required(),
  surname: Joi.string().min(3).max(20).required(),
});

const validateRegisterUser = (
  req: Request<User>,
  res: Response,
  next: NextFunction
) => {
  const validation = registerUserSchema.validate(req.body);

  if (validation.error) {
    const errorMessage = validation.error.details.map((err) => err.message);
    return res.status(500).json(errorMessage);
  }
  next();
};

export default validateRegisterUser;
