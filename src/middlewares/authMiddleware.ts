import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

export const authenticateToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authHeader = req.headers["authorization"];
  console.log("middleware run");
  const token = authHeader?.split(" ")[1];
  console.log(authHeader);
  console.log(token);

  if (!token) return res.status(401);

  jwt.verify(token, process.env.SECRET_TOKEN as string, (err, user) => {
    if (err) {
      console.log("ERROR verifi token");
      return res.status(403);
    }

    // req.user = user;
    res.locals.user = user;
    console.log(user);
  });
  next();
};
