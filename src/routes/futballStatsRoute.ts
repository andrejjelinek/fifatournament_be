import express from "express";
import futballStatsController from "../controllers/futballStatsController";
import multer from "multer";

const router = express.Router();
const upload = multer({ storage: multer.memoryStorage() });

router.post("/uploadPlayerData", upload.single("file"), futballStatsController.uploadPlayerData);

export default router;
