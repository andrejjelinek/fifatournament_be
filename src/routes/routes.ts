import express from "express";
// import authRouter from "./authRoute";
import tournamentRouter from "./tournamentRoute";
import futballStatsRouter from "./futballStatsRoute";

const router = express.Router();

// router.use(authRouter);
router.use(tournamentRouter);
router.use(futballStatsRouter);

export default router;
