import express from "express";
import tournamentController from "../controllers/tournamentController";

const router = express.Router();
router.post("/newTournament", tournamentController.createTournament);
router.get("/getTournaments", tournamentController.getTournaments);
router.get(
  "/matches/:tournamentId/:isFromLoosed",
  tournamentController.getTournamentMatches
);
router.post("/newMatch", tournamentController.createNewMatch);
router.post("/updateMatch/:isFromLoosed", tournamentController.updateMatch);
router.post("/updatePlayersInfo", tournamentController.updatePlayersInfo);
router.get(
  "/points/:tournamentId/:loosedType",
  tournamentController.getPlayersPointByTournament
);

export default router;
